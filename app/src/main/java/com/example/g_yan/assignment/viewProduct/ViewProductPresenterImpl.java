package com.example.g_yan.assignment.viewProduct;

import com.example.g_yan.assignment.core.IProductDetail;

import java.util.ArrayList;

public class ViewProductPresenterImpl implements ViewProductPresenter {
    ViewProductModel productModel;
    ViewProductView productView;

    public ViewProductPresenterImpl(ViewProductView productView) {
        this.productView = productView;
        productModel = new ViewProductModelImpl(this);
    }

    @Override
    public void getProducts() {
        productModel.getProducts();
    }

    @Override
    public void onGetProductSuccess(ArrayList<IProductDetail> products) {
        productView.onGetProductSuccess(products);
    }

    @Override
    public void onGetProductFailure(String error) {
        productView.onGetProductFailure(error);
    }
}
