package com.example.g_yan.assignment.addProduct;

import com.example.g_yan.assignment.core.IProductDetail;

/**
 * Created by g-yan on 26/6/18.
 */

public class AddProductPresenterImpl implements AddProductPresenter {

    AddProductView addProductView;
    AddProductModel addProductModel;

    public AddProductPresenterImpl(AddProductView addProductView) {
        this.addProductView = addProductView;
        addProductModel = new AddProductModelImpl(this);
    }

    @Override
    public void addItem(IProductDetail productDetail) {
        if(addProductModel ==null)
            return;
        addProductModel.addtoCart(productDetail);
    }

    @Override
    public void onFailure(String error) {
        if(addProductView ==null)
            return;
        addProductView.onFailure(error);
    }

    @Override
    public void onSuccess() {
        if(addProductView ==null)
            return;
        addProductView.onSuccess();
    }
}
