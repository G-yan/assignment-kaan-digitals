package com.example.g_yan.assignment.core;

public interface IProductDetail {
    String TABLE_NAME = "products";
    String PRODUCT_NAME = "prod_name";
    String PRODUCT_PRICE = "prod_price";
    String PRODUCT_IMAGE1 = "prod_image1";
    String PRODUCT_IMAGE2 = "prod_image2";
    String PRODUCT_THUMB_IMAGE1 = "prod_thumb_image1";
    String PRODUCT_THUMB_IMAGE2 = "prod_thumb_image2";
    String COLUMN_ID = "id";

    String CREATE_TABLE = "CREATE TABLE " + TABLE_NAME + "("
            + COLUMN_ID + " INTEGER PRIMARY KEY AUTOINCREMENT,"
            + PRODUCT_NAME + " TEXT,"
            + PRODUCT_PRICE + " INTEGER,"
            + PRODUCT_IMAGE1 + " TEXT,"
            + PRODUCT_IMAGE2 + " TEXT,"
            + PRODUCT_THUMB_IMAGE1 + " TEXT,"
            + PRODUCT_THUMB_IMAGE2 + " TEXT"
            + ")";

    String getProductName();

    int getProductPrice();

    String getImageUrl1();

    String getImageUrl2();

    String getImageThumbUrl1();

    String getImageThumbUrl2();

    void setProductName(String productName);

    void setProductPrice(int productPrice);

    void setImageUrl1(String imageUrl1);

    void setImageUrl2(String imageUrl2);

    void setImageThumbUrl1(String imageUrl1);

    void setImageThumbUrl2(String imageUrl2);
}
