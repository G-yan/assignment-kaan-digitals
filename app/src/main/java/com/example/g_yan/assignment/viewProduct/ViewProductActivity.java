package com.example.g_yan.assignment.viewProduct;

import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.Toast;

import com.example.g_yan.assignment.R;
import com.example.g_yan.assignment.core.IProductDetail;

import java.util.ArrayList;

import static android.nfc.tech.MifareUltralight.PAGE_SIZE;

public class ViewProductActivity extends AppCompatActivity implements ViewProductView {
    private RecyclerView rvProductList;
    private ProductAdapter productAdapter;
    private LinearLayoutManager layoutManager;
    private boolean isLoading;
    private boolean isLastPage;
    private ArrayList<IProductDetail> productDetails;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_view_product);
        initView();
        getProducts();
    }

    private void initView() {
        ActionBar supportActionBar = getSupportActionBar();
        supportActionBar.setTitle("View Product");

        rvProductList = findViewById(R.id.rv_products);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getApplicationContext());
        rvProductList.setLayoutManager(mLayoutManager);
        rvProductList.setItemAnimator(new DefaultItemAnimator());
        DividerItemDecoration dividerItemDecoration = new DividerItemDecoration(rvProductList.getContext(),
                LinearLayoutManager.VERTICAL);
        rvProductList.addItemDecoration(dividerItemDecoration);

    }


    private RecyclerView.OnScrollListener recyclerViewOnScrollListener = new RecyclerView.OnScrollListener() {
        @Override
        public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
            super.onScrollStateChanged(recyclerView, newState);
        }

        @Override
        public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
            super.onScrolled
                    (recyclerView, dx, dy);
            int visibleItemCount = layoutManager.getChildCount();
            int totalItemCount = layoutManager.getItemCount();
            int firstVisibleItemPosition = layoutManager.findFirstVisibleItemPosition();

            if (!isLoading && !isLastPage) {
                if ((visibleItemCount + firstVisibleItemPosition) >= totalItemCount
                        && firstVisibleItemPosition >= 0
                        && totalItemCount >= PAGE_SIZE) {
                    getProducts();
                }
            }
        }
    };

    @Override
    public void onGetProductSuccess(ArrayList<IProductDetail> productDetails) {
        isLoading=false;
        if (productDetails.size() <= 0)
            isLastPage = true;
        if (productAdapter == null) {
            this.productDetails = productDetails;
            productAdapter = new ProductAdapter(this.productDetails, this);
            layoutManager = new LinearLayoutManager(this);
            rvProductList.setLayoutManager(layoutManager);
//            rvProductList.addOnScrollListener(recyclerViewOnScrollListener);
            rvProductList.setAdapter(productAdapter);
        } else {
            this.productDetails.addAll(productDetails);
            productAdapter.notifyDataSetChanged();
        }


    }

    @Override
    public void onGetProductFailure(String error) {
        Toast.makeText(this, error, Toast.LENGTH_SHORT).show();

    }

    @Override
    public void getProducts() {
        ViewProductPresenter productPresenter = new ViewProductPresenterImpl(this);
        productPresenter.getProducts();
        isLoading = true;
    }
}
