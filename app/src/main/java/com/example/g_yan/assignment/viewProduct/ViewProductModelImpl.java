package com.example.g_yan.assignment.viewProduct;

import com.example.g_yan.assignment.core.IProductDetail;
import com.example.g_yan.assignment.database.CartDatabaseManager;

import java.util.ArrayList;

public class ViewProductModelImpl implements ViewProductModel {
    ViewProductPresenter productPresenter;
    int start = 0, end = 5;


    public ViewProductModelImpl(ViewProductPresenter productPresenter) {
        this.productPresenter = productPresenter;
    }

    @Override
    public void getProducts() {
        ArrayList<IProductDetail> products = CartDatabaseManager.getInstance().getProducts();

        if (products == null)
            productPresenter.onGetProductFailure("No Product found");
        else {
            productPresenter.onGetProductSuccess(products);
            start = end;
            end = end + 5;
        }


    }
}
