package com.example.g_yan.assignment.viewProduct;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.support.design.widget.TabLayout;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.g_yan.assignment.R;
import com.example.g_yan.assignment.core.IProductDetail;

import java.util.ArrayList;

public class ProductAdapter extends RecyclerView.Adapter<ProductAdapter.ProductViewHolder> {
    private ArrayList<IProductDetail> productDetails;
    private Context mContext;

    public ProductAdapter(ArrayList<IProductDetail> productDetails, Context mContext) {
        this.productDetails = productDetails;
        this.mContext = mContext;
    }

    @Override
    public ProductViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.product_list_row, parent, false);
        return new ProductViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(ProductViewHolder holder, int position) {
        IProductDetail productDetail = productDetails.get(position);
        holder.prodName.setText(productDetail.getProductName());
        holder.prodPrice.setText("Price Rs " + productDetail.getProductPrice());

        ArrayList<String> imgUrls = new ArrayList<>();
        if (!TextUtils.isEmpty(productDetail.getImageThumbUrl1()))
            imgUrls.add(productDetail.getImageThumbUrl1());
        if (!TextUtils.isEmpty(productDetail.getImageThumbUrl2()))
            imgUrls.add(productDetail.getImageThumbUrl2());

        ProductImageAdapter imageAdapter = new ProductImageAdapter(imgUrls);
        holder.prodImgviewPager.setAdapter(imageAdapter);
        holder.indicator.setupWithViewPager(holder.prodImgviewPager);

    }

    @Override
    public int getItemCount() {
        return productDetails == null ? 0 : productDetails.size();
    }

    class ProductViewHolder extends RecyclerView.ViewHolder {
        private TextView prodName, prodPrice;
        private ViewPager prodImgviewPager;
        private TabLayout indicator;

        public ProductViewHolder(View itemView) {
            super(itemView);
            prodName = itemView.findViewById(R.id.tv_prod_name);
            prodPrice = itemView.findViewById(R.id.tv_prod_price);
            prodImgviewPager = itemView.findViewById(R.id.prod_pager);
            indicator = itemView.findViewById(R.id.indicator);
        }
    }


    class ProductImageAdapter extends PagerAdapter {
        ArrayList<String> imgUrls;

        public ProductImageAdapter(ArrayList<String> imgUrls) {
            this.imgUrls = imgUrls;
        }

        @Override
        public int getCount() {
            return imgUrls == null ? 0 : imgUrls.size();
        }

        @Override
        public boolean isViewFromObject(View view, Object object) {
            return view == object;
        }

        @Override
        public Object instantiateItem(ViewGroup collection, int position) {
            LayoutInflater inflater = LayoutInflater.from(mContext);
            ViewGroup layout = (ViewGroup) inflater.inflate(R.layout.prod_banner, collection, false);
            collection.addView(layout);

            ImageView prodBanner = layout.findViewById(R.id.prod_banner);
            prodBanner.setImageBitmap(BitmapFactory.decodeFile(imgUrls.get(position)));
            return layout;
        }

        @Override
        public void destroyItem(ViewGroup collection, int position, Object view) {
            collection.removeView((View) view);
        }

    }
}
