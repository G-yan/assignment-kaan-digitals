package com.example.g_yan.assignment.utils;

public interface PermissionCallBack {
    void permissionApproved(String... permissions);

    void permissionDenied(String... permissionName);

}
