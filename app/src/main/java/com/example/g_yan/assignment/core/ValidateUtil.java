package com.example.g_yan.assignment.core;

import android.text.TextUtils;

public class ValidateUtil {

    public static String validate(IProductDetail productDetail){
        if(TextUtils.isEmpty(productDetail.getProductName()))
            return "please enter a product name";

        if(productDetail.getProductName().length()>100)
            return "please enter product name less than 100 characters";

        if(TextUtils.isEmpty(productDetail.getImageUrl1()) && TextUtils.isEmpty(productDetail.getImageUrl2()))
            return "please capture at least one product image";

        if(productDetail.getProductPrice()<=0)
            return "please enter a valid price";


        return "";
    }
}
