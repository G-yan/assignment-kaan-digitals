package com.example.g_yan.assignment.viewProduct;

import com.example.g_yan.assignment.core.IProductDetail;

import java.util.ArrayList;

public interface ViewProductView {
    void onGetProductSuccess(ArrayList<IProductDetail> productDetails);
    void onGetProductFailure(String error);
    void getProducts();
}
