package com.example.g_yan.assignment.utils;

import android.Manifest;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.BottomSheetDialogFragment;
import android.support.v4.content.ContextCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import com.example.g_yan.assignment.MainApplication;
import com.example.g_yan.assignment.R;

import java.util.ArrayList;
import java.util.List;

public class PermissionDialog extends BottomSheetDialogFragment {
    private PermissionCallBack callback;
    private Button btnAllow, btnDeny;
    private static final int REQUEST_FOR_PERMISSION = 3;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.dialogue_permission, container);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        if(isPermissionGRanted(Manifest.permission.CAMERA)){
            callback.permissionApproved();
            dismiss();
        }
        else {
            List<String> permissionList = new ArrayList<>();
            permissionList.add(Manifest.permission.CAMERA);
            requestPermissions((permissionList.toArray(new String[permissionList.size()])), REQUEST_FOR_PERMISSION);
        }

    }



    public void setCallback(PermissionCallBack callback) {
        this.callback = callback;
    }

    public static PermissionDialog newInstance() {
        return new PermissionDialog();
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (requestCode == REQUEST_FOR_PERMISSION) {

            List<String> approvedList = new ArrayList<>();
            List<String> deniedList = new ArrayList<>();
            for (int i = 0; i < grantResults.length; i++) {
                if (grantResults[i] == PackageManager.PERMISSION_GRANTED)
                    approvedList.add(permissions[i]);
                else deniedList.add(permissions[i]);
            }
            callback.permissionDenied();
            callback.permissionApproved();
            dismiss();
        }
    }

    public static boolean isPermissionGRanted(String permissionsName) {
        return PackageManager.PERMISSION_GRANTED == ContextCompat.checkSelfPermission(MainApplication.getContext(), permissionsName) ? true : false;
    }
}
