package com.example.g_yan.assignment;

import android.app.Application;
import android.content.Context;

/**
 * Created by g-yan on 26/6/18.
 */

public class MainApplication extends Application {

    private static Context context;

    @Override
    public void onCreate() {
        super.onCreate();
        context = this;
    }

    public static Context getContext() {
        return context;
    }
}
