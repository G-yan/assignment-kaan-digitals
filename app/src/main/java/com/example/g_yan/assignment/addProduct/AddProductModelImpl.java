package com.example.g_yan.assignment.addProduct;

import android.text.TextUtils;

import com.example.g_yan.assignment.core.IProductDetail;
import com.example.g_yan.assignment.core.ValidateUtil;
import com.example.g_yan.assignment.database.CartDatabaseManager;

/**
 * Created by g-yan on 26/6/18.
 */

public class AddProductModelImpl implements AddProductModel {

    AddProductPresenter addProductPresenter;

    public AddProductModelImpl(AddProductPresenter addProductPresenter) {
        this.addProductPresenter = addProductPresenter;
    }

    @Override
    public void addtoCart(IProductDetail productDetail) {
        String validationString = ValidateUtil.validate(productDetail);
        if (TextUtils.isEmpty(validationString)) {
            long l = CartDatabaseManager.getInstance().insertProduct(productDetail);
            addProductPresenter.onSuccess();
        } else {
            addProductPresenter.onFailure(validationString);
        }
    }
}
