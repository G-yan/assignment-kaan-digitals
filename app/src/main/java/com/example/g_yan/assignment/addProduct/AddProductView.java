package com.example.g_yan.assignment.addProduct;

/**
 * Created by g-yan on 26/6/18.
 */

public interface AddProductView {
    void addItem();
    void onFailure(String error);
    void onSuccess();
}
