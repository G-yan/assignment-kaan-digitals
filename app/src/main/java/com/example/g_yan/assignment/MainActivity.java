package com.example.g_yan.assignment;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import com.example.g_yan.assignment.addProduct.AddProductActivity;
import com.example.g_yan.assignment.core.IProductDetail;
import com.example.g_yan.assignment.database.CartDatabaseManager;
import com.example.g_yan.assignment.viewProduct.ViewProductActivity;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity {

    private Button addToCart, viewCart;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        initView();

    }

    private void initView() {
        addToCart = findViewById(R.id.add_to_cart);
        viewCart = findViewById(R.id.view_cart);

        addToCart.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MainActivity.this, AddProductActivity.class);
                startActivity(intent);
            }
        });

        viewCart.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MainActivity.this, ViewProductActivity.class);
                startActivity(intent);
            }
        });
    }


}
