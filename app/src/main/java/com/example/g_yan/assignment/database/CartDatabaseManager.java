package com.example.g_yan.assignment.database;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import com.example.g_yan.assignment.MainApplication;
import com.example.g_yan.assignment.core.IProductDetail;
import com.example.g_yan.assignment.core.ProductDetail;

import java.util.ArrayList;

/**
 * Created by g-yan on 26/6/18.
 */

public class CartDatabaseManager extends SQLiteOpenHelper {
    private static int VERSION = 1;
    private static CartDatabaseManager databaseManager;
    private SQLiteDatabase dataBase;

    public static CartDatabaseManager getInstance() {
        if (databaseManager == null)
            databaseManager = new CartDatabaseManager(MainApplication.getContext());
        return databaseManager;
    }

    public CartDatabaseManager(Context context) {
        super(context, "assignment.db", null, VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(IProductDetail.CREATE_TABLE);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

    }

    private SQLiteDatabase getDatabse() {
        if (dataBase == null || (!dataBase.isOpen()))
            dataBase = databaseManager.getWritableDatabase();
        return dataBase;
    }

    public void closeDatabse() {
        if (dataBase != null && (dataBase.isOpen()))
            dataBase.close();
    }

    public long insertProduct(IProductDetail productDetail) {
        dataBase = getDatabse();
        ContentValues values = new ContentValues();
        values.put(IProductDetail.PRODUCT_NAME, productDetail.getProductName());
        values.put(IProductDetail.PRODUCT_PRICE, productDetail.getProductPrice());
        values.put(IProductDetail.PRODUCT_IMAGE1, productDetail.getImageUrl1());
        values.put(IProductDetail.PRODUCT_IMAGE2, productDetail.getImageUrl2());
        values.put(IProductDetail.PRODUCT_THUMB_IMAGE1, productDetail.getImageThumbUrl1());
        values.put(IProductDetail.PRODUCT_THUMB_IMAGE2, productDetail.getImageThumbUrl2());

        // insert row
        long id = dataBase.insert(IProductDetail.TABLE_NAME, null, values);

        // close db connection
        dataBase.close();

        // return newly inserted row id
        return id;
    }

//    public ArrayList<IProductDetail> getProducts(int start, int end) {
    public ArrayList<IProductDetail> getProducts() {
        ArrayList<IProductDetail> productDetails = new ArrayList<>();
        dataBase = getDatabse();
//        String whereClause = "id > ? AND id <= ?";
//        String[] whereArgs = new String[] {
//                String.valueOf(start),
//                String.valueOf(end)
//        };
        Cursor cursor = dataBase.query(IProductDetail.TABLE_NAME,
                null, null, null, null, null, null, null);

        if (cursor != null &&cursor.moveToFirst())
        do {
            IProductDetail productDetail = new ProductDetail();
            productDetail.setProductName(cursor.getString(cursor.getColumnIndex(IProductDetail.PRODUCT_NAME)));
            productDetail.setImageUrl1(cursor.getString(cursor.getColumnIndex(IProductDetail.PRODUCT_IMAGE1)));
            productDetail.setImageUrl2(cursor.getString(cursor.getColumnIndex(IProductDetail.PRODUCT_IMAGE2)));
            productDetail.setImageThumbUrl1(cursor.getString(cursor.getColumnIndex(IProductDetail.PRODUCT_THUMB_IMAGE1)));
            productDetail.setImageThumbUrl2(cursor.getString(cursor.getColumnIndex(IProductDetail.PRODUCT_THUMB_IMAGE2)));
            productDetail.setProductPrice(cursor.getInt(cursor.getColumnIndex(IProductDetail.PRODUCT_PRICE)));

            productDetails.add(productDetail);
        }while (cursor.moveToNext());
        cursor.close();
        dataBase.close();

        return productDetails;
    }
}
