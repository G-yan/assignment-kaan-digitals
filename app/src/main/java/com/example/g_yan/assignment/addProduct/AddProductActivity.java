package com.example.g_yan.assignment.addProduct;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.v4.content.FileProvider;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import com.example.g_yan.assignment.R;
import com.example.g_yan.assignment.core.IProductDetail;
import com.example.g_yan.assignment.core.ProductDetail;
import com.example.g_yan.assignment.utils.PermissionCallBack;
import com.example.g_yan.assignment.utils.PermissionDialog;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class AddProductActivity extends AppCompatActivity implements AddProductView {

    private static final String TAG = AddProductActivity.class.getSimpleName();
    static final int REQUEST_TAKE_PHOTO = 1;
    private boolean image1Clicked = false;

    private Button add;
    private EditText prodName, prodPrice;
    private ImageView image1, image2;

    private String imageUrl1 = "", imageUrl2 = "",imageThumbUrl1 = "", imageThumbUrl2 = "";

    private AddProductPresenter cartPresenter;
    private Uri photoURI;
    String mCurrentPhotoPath;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_addto_cart);
        initView();
    }

    private void initView() {
        ActionBar supportActionBar = getSupportActionBar();
        supportActionBar.setTitle("Add Product");

        add = findViewById(R.id.add);
        prodName = findViewById(R.id.prod_name);
        prodPrice = findViewById(R.id.prod_price);
        image1 = findViewById(R.id.image1);
        image2 = findViewById(R.id.image2);

        add.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                addItem();
            }
        });

        image1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                image1Clicked = true;
                initiateCamera();
            }
        });

        image2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                image1Clicked = false;
                initiateCamera();
            }
        });

        cartPresenter = new AddProductPresenterImpl(this);
    }

    @Override
    public void addItem() {
        IProductDetail detail = new ProductDetail();

        String prodNameText = prodName.getText() == null ? "" : prodName.getText().toString();
        String priceText = prodPrice.getText() == null ? "" : prodPrice.getText().toString();
        int prodPriceText = TextUtils.isEmpty(priceText) ? 0 : Integer.valueOf(priceText);

        detail.setProductName(prodNameText);
        detail.setProductPrice(prodPriceText);

        detail.setImageUrl1(imageUrl1);
        detail.setImageUrl2(imageUrl2);
        detail.setImageThumbUrl1(imageThumbUrl1);
        detail.setImageThumbUrl2(imageThumbUrl2);

        cartPresenter.addItem(detail);
    }

    @Override
    public void onFailure(String error) {
        Toast.makeText(this, error, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onSuccess() {
        Toast.makeText(this, "product added successfully", Toast.LENGTH_SHORT).show();
        this.finish();
    }

    private void initiateCamera() {
        final PermissionDialog permissionDialog = PermissionDialog.newInstance();
        permissionDialog.setCallback(new PermissionCallBack() {

            @Override
            public void permissionApproved(String... permissions) {
                dispatchTakePictureIntent();
            }

            @Override
            public void permissionDenied(String... permissionName) {

            }

        });
        permissionDialog.show(getSupportFragmentManager(), "");
    }

    private File createImageFile() throws IOException {
        // Create an image file name
        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
        String imageFileName = "JPEG_" + timeStamp + "_";
        File storageDir = getExternalFilesDir(Environment.DIRECTORY_PICTURES);
        File image = File.createTempFile(
                imageFileName,  /* prefix */
                ".jpg",         /* suffix */
                storageDir      /* directory */
        );

        // Save a file: path for use with ACTION_VIEW intents
        mCurrentPhotoPath = image.getAbsolutePath();
        return image;
    }

    private void dispatchTakePictureIntent() {
        Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        if (takePictureIntent.resolveActivity(getPackageManager()) != null) {
            File photoFile = null;
            try {
                photoFile = createImageFile();
            } catch (IOException ex) {
            }
            if (photoFile != null) {
                photoURI = FileProvider.getUriForFile(this,
                        "com.example.g_yan.assignment.fileprovider",
                        photoFile);
                takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT, photoURI);
                startActivityForResult(takePictureIntent, REQUEST_TAKE_PHOTO);
            }
        }
    }

    private void setPic() {
        try {
            if (photoURI != null) {

                String path = "/storage/emulated/0/Android/data/com.example.g_yan.assignment/files/Pictures/";
                String photoUriPath = path + (photoURI.getPath().split("/my_images/")[1]);

                Bitmap bitmap = BitmapFactory.decodeFile(photoUriPath);
                if (image1Clicked){
                    image1.setImageBitmap(bitmap);
                    imageUrl1 = photoUriPath;
                    imageThumbUrl1 = saveThumbImage(bitmap,imageUrl1);
                }
                else{
                    image2.setImageBitmap(bitmap);
                    imageUrl2 = photoUriPath;
                    imageThumbUrl2 = saveThumbImage(bitmap,imageUrl2);

                }
            }
        } catch (Exception e) {
            Log.e("URI EXCEPTION", "handle exception");
        }
    }

    private String saveThumbImage(Bitmap bitmap, String imageUrl) {

        byte[] imageData = null;
        String thumImgUrl = imageUrl.replace(".jpg","_thumb.jpg");
        FileOutputStream baos = null;
        try
        {

            final int THUMBNAIL_SIZE = 128;

            FileInputStream fis = new FileInputStream(imageUrl);
            Bitmap imageBitmap = BitmapFactory.decodeStream(fis);

            imageBitmap = Bitmap.createScaledBitmap(imageBitmap, THUMBNAIL_SIZE, THUMBNAIL_SIZE, false);

            baos = new FileOutputStream(thumImgUrl);
            imageBitmap.compress(Bitmap.CompressFormat.JPEG, 100, baos);

        }
        catch(Exception ex) {

        }finally {
            try {
                if (baos != null) {
                    baos.close();
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        return thumImgUrl;
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == REQUEST_TAKE_PHOTO) {
            Log.d(TAG, mCurrentPhotoPath);
            Toast.makeText(this, mCurrentPhotoPath, Toast.LENGTH_SHORT).show();
            setPic();
        }
    }

}