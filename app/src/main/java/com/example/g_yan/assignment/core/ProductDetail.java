package com.example.g_yan.assignment.core;

/**
 * Created by g-yan on 26/6/18.
 */

public class ProductDetail implements IProductDetail {
    private String productName;
    private int productPrice;
    private String imageUrl1;
    private String imageUrl2;
    private String imageThumbUrl1;
    private String imageThumbUrl2;

    public String getProductName() {
        return productName;
    }

    public int getProductPrice() {
        return productPrice;
    }

    public String getImageUrl1() {
        return imageUrl1;
    }

    public String getImageUrl2() {
        return imageUrl2;
    }

    public String getImageThumbUrl1() {
        return imageThumbUrl1;
    }

    public String getImageThumbUrl2() {
        return imageThumbUrl2;
    }

    public void setProductName(String productName) {
        this.productName = productName;
    }

    public void setProductPrice(int productPrice) {
        this.productPrice = productPrice;
    }

    public void setImageUrl1(String imageUrl1) {
        this.imageUrl1 = imageUrl1;
    }

    public void setImageUrl2(String imageUrl2) {
        this.imageUrl2 = imageUrl2;
    }

    public void setImageThumbUrl1(String imageThumbUrl1) {
        this.imageThumbUrl1 = imageThumbUrl1;
    }

    public void setImageThumbUrl2(String imageThumbUrl2) {
        this.imageThumbUrl2 = imageThumbUrl2;
    }
}
