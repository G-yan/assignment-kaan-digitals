package com.example.g_yan.assignment.addProduct;

import com.example.g_yan.assignment.core.IProductDetail;

/**
 * Created by g-yan on 26/6/18.
 */

public interface AddProductModel {
    void addtoCart(IProductDetail productDetail);
}
